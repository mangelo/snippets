Change the text and background color in grub
----------------------------------------------

* /etc/grub.d/99_colors
    #!/bin/sh
    exec tail -n +3 $0
    set menu_color_highlight=dark-gray/black
    set menu_color_normal=black/black
    set color_normal=dark-gray/black

* chmod 755 /etc/grub.d/99_colors

* update-grub


Change the timeout if boot fails
----------------------------------

* /etc/default/grub
    GRUB_TIMEOUT=5
    GRUB_RECORDFAIL_TIMEOUT=$GRUB_TIMEOUT

* update-grub
