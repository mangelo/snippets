##
## Author: Jeremy Grossmann (2009)
## Contributor: J. Pedro Flor (28 january 2010)
##

FIRST_START=no
if test ! -e /mnt/disk0/lina_monitor
then
 cd /asa/scripts/
 echo "d" > /asa/scripts/fdisk.pf.in
 echo "o" >> /asa/scripts/fdisk.pf.in
 echo "n" >> /asa/scripts/fdisk.pf.in
 echo "p" >> /asa/scripts/fdisk.pf.in
 echo "1" >> /asa/scripts/fdisk.pf.in
 echo "1" >> /asa/scripts/fdisk.pf.in
 echo ""  >> /asa/scripts/fdisk.pf.in
 echo "t" >> /asa/scripts/fdisk.pf.in
 echo "4" >> /asa/scripts/fdisk.pf.in
 echo "w" >>/asa/scripts/fdisk.pf.in

 echo ""
 echo -n "Initializing partition..."
 /sbin/fdisk /dev/hda < /asa/scripts/fdisk.pf.in > /dev/null 2> /dev/null
 echo "done"

 echo ""
 echo -n "Formating and mounting partition..."
 mkdosfs -F 16 /dev/hda1 > /dev/null 2> /dev/null
 mount -o umask=0000,noatime,check=s,shortname=mixed /dev/hda1 /mnt/disk0 > /dev/null 2> /dev/null
 echo "done"
 echo ""

 cp /asa/bin/lina /mnt/disk0/lina
 cp /asa/bin/lina_monitor /mnt/disk0/lina_monitor
 FIRST_START=yes
fi

# load drivers
modprobe e100
modprobe e1000
ifconfig eth0 up
ifconfig eth1 up
ifconfig eth2 up
ifconfig eth3 up
ifconfig eth4 up
ifconfig eth5 up

if test $FIRST_START = yes
then
 echo ""
 echo "          Cisco ASA with <NO> Multiple Security Contexts"
 echo "          =============================================="
 echo ""
 echo "This is your first boot, please wait about 2 minutes for 'disk0' creation"
 echo "and then execute the following commands inside the Linux prompt:"
 echo ""
 echo " # cd /mnt/disk0"
 echo " # /mnt/disk0/lina_monitor"
 echo ""
 echo ""
 echo ""
 echo "Please note to use the following command under ASA to save your configs:"
 echo ""
 echo " ciscoasa(config)# boot config disk0:/.private/startup-config"
 echo " ciscoasa(config)# copy running-config disk0:/.private/startup-config"
 echo ""
 echo ""
 echo ""
 echo "To get webvpn working, execute the following commands:"
 echo ""
 echo " ciscoasa# mkdir disk0:/var"
 echo " ciscoasa# mkdir disk0:/var/log"
 echo " ciscoasa# mkdir disk0:/csco_config"
 echo " ciscoasa# mkdir disk0:/csco_config/97"
 echo " ciscoasa# mkdir disk0:/csco_config/97/webcontent"
 echo ""
 echo "          ( Powered by Pedro Flor )"
 echo "          ( pedro.flor@gmail.com  )"
 echo ""
 exit
fi

echo ""
echo ""
echo "Starting Cisco ASA with <NO> Multiple Security Contexts..."
echo ""

cd /mnt/disk0
/mnt/disk0/lina_monitor
