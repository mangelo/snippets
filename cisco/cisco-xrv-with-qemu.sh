### One-time:

# Uncompress the OVA file.
    tar xvf iosxrv-k9-demo-ova-601.ova

# Convert the vmdk disk to qcow2 format.
    qemu-img convert iosxrv-demo.vmdk -O qcow2 iosxrv-k9-demo-601.qcow2

# Copy the qcow2 disk to a shared location.
    cp iosxrv-k9-demo-601.qcow2 /opt/ciscoxr/iosxrv-k9-demo-601.qcow2

# Make the qcow2 disk readonly.
    chmod 444 /opt/ciscoxr/iosxrv-k9-demo-601.qcow2

### For every router:

# Create the backing file to store the configuration.
    qemu-img create -f qcow2 -b /opt/ciscoxr/iosxrv-k9-demo-601.qcow2 router1.qcow2

# Start the router.
    qemu-system-x86_64 \
        -name ROUTER1 \
        -m 2048 \
        -smp cpus=1 \
        -enable-kvm \
        -hda router1.qcow2 \
        -serial telnet::2000,server,nowait \
        -monitor tcp:127.0.0.1:4000,server,nowait \
        -display none \
        -daemonize \
        -net none \
        -net nic,model=e1000,vlan=1,macaddr=00:01:00:ff:01:01 \
        -net nic,model=e1000,vlan=2,macaddr=00:01:00:ff:01:02 \
        -net nic,model=e1000,vlan=3,macaddr=00:01:00:ff:01:03 \
        -net nic,model=e1000,vlan=4,macaddr=00:01:00:ff:01:04 \
        -net tap,vlan=1,ifname=tapxr,script=no,downscript=no \
        -net socket,vlan=2,udp=127.0.0.1:9012,localaddr=:9002 \
        -net socket,vlan=3,udp=127.0.0.1:9013,localaddr=:9003 \
        -net socket,vlan=4,udp=127.0.0.1:9014,localaddr=:9004

# For management with telnet or ssh over MgmtEth0/0/CPU0/0 interface.
su - root -c 'tunctl -u username -t tapxr > /dev/null ; ifconfig tapxr <IP_ADDR> netmask <MASK> up'

### Useful commands.

# Info of the qcow2 disk.
    qemu-img info router1.qcow2
