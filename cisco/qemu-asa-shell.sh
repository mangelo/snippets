#======================================================================
# -- ASA9v --
#======================================================================

#
# Serial console:
#   Start with -curses
#    copy disk0:/coredumpinfo/coredump.cfg disk0:/use_ttyS0
#   Start with -display none
#

qemu-system-x86_64 \
    -daemonize \
    -L /usr/share/qemu \
    -m 1024M \
    -smp cpus=1 \
    -enable-kvm \
    -boot order=c \
    -drive file=asav961.qcow2,if=ide,index=0,media=disk \
    -monitor none -display none -vga none \
    -serial telnet::2000,server,nowait \
    -net none \
    -net nic,vlan=1,macaddr=00:00:ab:e3:eb:01,model=e1000 -net socket,vlan=1,udp=127.0.0.1:9000,localaddr=:9001 \
    -net nic,vlan=2,macaddr=00:00:ab:e3:eb:02,model=e1000 -net socket,vlan=2,udp=127.0.0.1:9002,localaddr=:9003 \
    -net nic,vlan=3,macaddr=00:00:ab:e3:eb:03,model=e1000 -net socket,vlan=3,udp=127.0.0.1:9004,localaddr=:9005 \
    -net nic,vlan=4,macaddr=00:00:ab:e3:eb:04,model=e1000 -net socket,vlan=4,udp=127.0.0.1:9006,localaddr=:9007


#======================================================================
# -- ASA 8.0(2) --
#======================================================================

#
# Create flash:
#   qemu-img create -f qcow2 asa802-flash.qcow2 128M
#

#
# Save config:
#   copy running-config flash:/.private/startup-config
#

ASAPID=/dev/shm/.asa802.pid

qemu-system-x86_64 \
    -daemonize \
    -L /usr/share/qemu \
    -m 1024M \
    -smp cpus=1 \
    -enable-kvm \
    -boot order=c \
    -hdachs 980,16,32 \
    -hda asa802-flash.qcow2 \
    -initrd asa802-k8.initrd \
    -kernel asa802-k8.kernel \
    -append "ide_generic.probe_mask=0x01 ide_core.chs=0.0:980,16,32 auto nousb console=ttyS0,9600 bigphysarea=65536 ide1=noprobe no-hlt -net nic" \
    -monitor none -display none -vga none \
    -pidfile "${ASAPID}" \
    -serial telnet::2000,server,nowait \
    -net none \
    -device e1000,mac=00:00:00:e3:eb:00,netdev=A00 -netdev socket,id=A00,udp=127.0.0.1:9000,localaddr=:9001 \
    -device e1000,mac=00:00:00:e3:eb:01,netdev=A01 -netdev socket,id=A01,udp=127.0.0.1:9002,localaddr=:9003 \
    -device e1000,mac=00:00:00:e3:eb:02,netdev=A02 -netdev socket,id=A02,udp=127.0.0.1:9004,localaddr=:9005 \
    -device e1000,mac=00:00:00:e3:eb:03,netdev=A03 -netdev socket,id=A03,udp=127.0.0.1:9006,localaddr=:9007 \
    -device e1000,mac=00:00:00:e3:eb:04,netdev=A04 -netdev socket,id=A04,udp=127.0.0.1:9008,localaddr=:9009 \
    -device e1000,mac=00:00:00:e3:eb:05,netdev=A05 -netdev socket,id=A05,udp=127.0.0.1:9010,localaddr=:9011

sleep 1
cpulimit --lazy --pid=`cat "${ASAPID}"` --limit=75 --background --quiet

#
# Extract kernel
#   https://ji8an.wordpress.com/2010/08/27/how-to-emulate-cisco-asa/
#

hexdump -C asa802-k8.bin > asa802-k8.hex
grep "1f 8b 08 00 1d" asa802-k8.hex                             #---> 001228b0  1f 8b 08 00 1d 3d ...
ls -la asa802-k8.bin                                            #---> 14524416
echo "14524416 ; ibase=16 ; last - 1228B0" | bc | tail -n 1     #---> 13334352
tail -c 13334352 asa802-k8.bin > asa802-k8.gz
gzip -d asa802-k8
mkdir tmp ; cd tmp
cpio -i --no-absolute-filenames --make-directories < ../asa802-k8   #---> as root.
cp vmlinuz ../asa802-k8.kernel
create /asa/scripts/first_start.sh
chmod 755 asa/scripts/first_start.sh
edit etc/init.d/rcS --> change /asa/bin/lina_monitor to /asa/scripts/first_start.sh
find . | cpio -o -H newc | gzip -9 > ../asa802-k8.initrd.gz     #---> as root
